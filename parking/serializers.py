from rest_framework import serializers

from parking.models import Visit

class CurrentUserSerializer(serializers.Serializer):
    
    id = serializers.ReadOnlyField()
    plate = serializers.CharField()
    date_time_in = serializers.DateTimeField(allow_null=True)
    date_time_out = serializers.DateTimeField(allow_null=True)
    visit = serializers.PrimaryKeyRelatedField(queryset=Visit.objects.all, allow_null=True)


