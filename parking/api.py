from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.generics import get_object_or_404
from dosiJetio.jetiodriver import jetioBarrier

from parking.serializers import CurrentUserSerializer
import environ


class CarEnterAPI(APIView):

    def post(self, request):
        car_serializer = CurrentUserSerializer(request.data)
        print(car_serializer)
        env = environ.Env()
        environ.Env.read_env()
        barr = jetioBarrier(env('JETIO_IP'), int(env('JETIO_PORT')), int(env('JETIO_ENTER_DOOR')))
        print(barr.outputPulseBit(bitToWrite=int(env('JETIO_ENTER_DOOR'))))
        return Response(car_serializer.data, status=status.HTTP_200_OK)


class CarExitAPI(APIView):
    
    def post(self, request):
        car_serializer = CurrentUserSerializer(request.data)
        print(car_serializer)
        env = environ.Env()
        environ.Env.read_env()
        barr = jetioBarrier(env('JETIO_IP'), int(env('JETIO_PORT')), int(env('JETIO_EXIT_DOOR')))
        print(barr.outputPulseBit(bitToWrite=int(env('JETIO_EXIT_DOOR'))))
        return Response(car_serializer.data, status=status.HTTP_200_OK)