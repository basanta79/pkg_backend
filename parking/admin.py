from django.contrib import admin
from parking.models import Plate, Driver, Visit, Current_users

admin.site.site_header = "Escubedo parquing admin"
admin.site.site_title = "Escubedo parquing admin portal"
admin.site.index_title = "Welcome to Escubedo parquing admin Portal"

admin.site.register(Plate)
admin.site.register(Driver)
admin.site.register(Visit)
admin.site.register(Current_users)
 