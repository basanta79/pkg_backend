from django.db import models
from django.contrib.auth.models import User
from datetime import datetime

EMPLOYEE = 'EM'
VISITOR = 'VI'

ROLE={
    (EMPLOYEE, 'Empleado'),
    (VISITOR, 'Visitante'),
}

class Plate(models.Model):
    number = models.CharField(max_length=10, unique=True)

    class Meta:
        verbose_name = "Matricula"

    def __str__(self):
        return self.number

class Driver(models.Model):
    plate_id = models.ManyToManyField(Plate, verbose_name="matricula")
    first_name = models.CharField(max_length=75 ,verbose_name="Nombre")
    last_name = models.CharField(max_length=125, verbose_name="Apellidos")
    email = models.EmailField(max_length=255, verbose_name="Correo electrónico")
    dni = models.CharField(max_length=10, verbose_name='D.N.I.')
    rol = models.CharField(max_length=2, verbose_name='ROL', choices=ROLE, default=VISITOR)

    class Meta:
        ordering = ['last_name', 'first_name']
        verbose_name = "Conductor"
        verbose_name_plural = "Conductores"

    def __str__(self):
        return self.first_name + ' ' + self.last_name

class Visit(models.Model):
    host = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name='Anfitrion')
    guest = models.ForeignKey(Driver, on_delete=models.CASCADE, verbose_name='Invitado')
    date_start = models.DateField(verbose_name='Fecha inicio', default=datetime.now)
    date_end = models.DateField(verbose_name='Fecha fin', default=datetime.now)

    def __str__(self):
        return self.guest.first_name
    
    class Meta:
        ordering = ['date_start']
        verbose_name = "Visita"

class Current_users(models.Model):
    plate_number = models.CharField(max_length=10, unique=True, verbose_name='Matricula')
    date_time_in = models.DateTimeField(verbose_name='Fecha y hora de entrada', default=datetime.now)
    date_time_out = models.DateTimeField(verbose_name='Fecha y hora de salida', null=True, blank=True)
    validated = models.BooleanField(default=False)
    visit = models.ForeignKey(Visit, null=True, blank=True, verbose_name='Visita id', related_name='visit', on_delete=models.CASCADE)

    def __str__(self):
        return self.plate_number
    
    class Meta:
        ordering = ['plate_number', 'date_time_in']
        verbose_name = "Usuario"

