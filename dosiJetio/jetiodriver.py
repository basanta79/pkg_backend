from dosiJetio.barrera import Barrera
from dosiJetio.socketBarrier import SocketBarrier
import time

class jetioBarrier(Barrera):
    _ip: str
    _port: str
    _bitOpen: str
    _sck: SocketBarrier

    def __init__(self, ip: str, port: int, bitOpen: int):
        self._ip = ip
        self._port = port 
        self._bitOpen = "%02d" %(bitOpen)
        self._sck = SocketBarrier(self._ip, self._port)

    def send(self, message: str):
        print(id(self._sck))
        ret = self._sck.sendToJetIO(message)
        return ret

    def outputBit(self, **kwargs):
        outLevel = '00'
        outBit = "%02d" %(kwargs.get('bitToWrite', 0))
        if (kwargs.get('level', False)):
            outLevel = 'FF'            
        ret = self.send('000000000006010500' + outBit + outLevel + '00')
        return 'Received', repr(ret)
    
    def outputPulseBit(self, **kwargs):
        outBit = "%02d" %(kwargs.get('bitToWrite', 0))
        posLevel = 'FF'
        negLevel = '00'
        if (kwargs.get('level', True)==False):
            posLevel = '00'            
            negLevel = 'FF'
        ret = self.send('000000000006010500' + outBit + posLevel + '00')
        print(ret)
        time.sleep(2)
        ret = self.send('000000000006010500' + outBit + negLevel + '00')
        print(ret)
        return 'Received', repr(ret)
    
    def _openBarrier_(self, **kwargs):
        ret = self.send('000000000006010500' + self._bitOpen + 'FF00')
        return 'Received', repr(ret)
    
    def _closeBarrier_(self, **kwargs):
        ret = self.send('000000000006010500' + self._bitOpen + '0000')
        return 'Received', repr(ret)